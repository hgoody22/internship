package com.company;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Keeps tracker of max number of development years for each origin year
 */
public class Tracker {
    Hashtable<Integer, Integer> list = new Hashtable<>();

    /**
     * Add new origin year to list
     * @param
     */
    void register(Integer originYear, Integer devYear) {
        if (list.containsKey(originYear)) ; //Do nothing as already registered year
        else list.put(originYear, 0);
    }

    /**
     * Increase size of origin domain if new development year is higher than previous
     * @param origin year
     * @param development year
     */
    void track(Integer origin, Integer development){
        try{
            int difference = development - origin;
            if(list.containsKey(origin)) {
                if(difference > list.get(origin)) list.replace(origin, difference);
            }
            else list.put(origin, difference);
        }
        catch (NullPointerException n){
            list.put(origin, development - origin);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Simple printer
     */
    void printTracker(){
        Integer key;
        Set<Integer> keys = list.keySet();
        Iterator<Integer> itr = keys.iterator();
        while(itr.hasNext()){
            key = itr.next();
            System.out.println("Key (origin year): " + key + ", Domain: " + list.get(key));
        }

    }

}
