package com.company;
import java.util.ArrayList;
import java.util.List;

/**
 * Each Origin object is a list of payments for each development year of some origin year
 */
class Origin {

    //id for each object
    private Integer originYear;
    private List<Double> accYears = new ArrayList<>();

    /**
     * Constructor method for Origin
     * @param originYear This is the start year for this object
     * @param firstPayment This is the first payment
     * @param devYear This is the development year in case incorrect order
     *
     */
    Origin(Integer originYear, Integer devYear, Double firstPayment){
        int difference = Math.abs(devYear) - Math.abs(originYear);
        for(int i = 0; i < difference; i++) accYears.add(0.0);
        this.originYear = Math.abs(originYear);
        if(originYear < 0) System.out.println("Using absolute value, year is " + this.originYear);
        accYears.add(difference, firstPayment);
    }

    /**
     * This allows one to add a new development year to the list of the same origin year
     * List is padded with a zero if no payment made in year
     * @param devYear Year of development
     * @param payment Amount paid in that year
     */
     void addDevYear(Integer devYear, Double payment){
        //Get difference
        int devs = devYear - originYear;
        //Pad list
        int padding = devs - accYears.size();
        for(int i = 0; i<padding; i++) accYears.add(0.0);
        accYears.add((devYear - originYear), payment);

//        for(int i = 0; i < devs; i++){
//            if(accYears.get(i) == null) accYears.add(i, 0);
//        }

    }

    /**
     * Accumulates values
     * @return List of values converted to string
     */
    List<String> sumUp(){
         List<String> total = new ArrayList<>();
         for(int i = 1; i < accYears.size(); i++){
             accYears.set(i, (accYears.get(i) + accYears.get(i-1)));
         }
        for (Double d : accYears) {
             total.add(String.valueOf(d));
        }
        return total;
    }

    /*
    public void printYears(){
        int devYear = 0;
        for (Double x : accYears){
            System.out.println("Dev year " + devYear + ": " + x.intValue());
            devYear++;
        }
    }

    int getOriginYear(){
        return originYear;
    }
*/
}
