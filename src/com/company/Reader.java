package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reader {
    List<String[]> fileLines = new ArrayList<>();
    String filename;
    Reader(String filename){
        this.filename = filename;
    }

    private String nada = "";
    private String comma = ",";

    /**
     * Reads in from file "filename"
     * Reads each line into array of strings broken at commas
     * @return list of strings, where each element corresponds to one line of array
     */
    List<String[]> readIn(){
        try(BufferedReader buff = new BufferedReader(new FileReader(filename))){

            while((nada = buff.readLine()) != null){
                //Filter out excess grammar
                String filtered = nada.replaceAll("[^a-zA-Z0-9,.-]", "");

                //Split up line at each comma
                String[] line = filtered.split(comma);

                //check valid line

                fileLines.add(line);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }

        return fileLines;
    }

}
