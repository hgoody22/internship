package com.company;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.*;

class Organiser {
    private HashMap<String, HashMap<Integer, Origin>> triangles = new HashMap<>();
    private List<String[]> fileLines = new ArrayList<>();
    private int counter = 0;
    private Set<ArrayList> outputs = new HashSet<>();
    private Tracker t = new Tracker();

    //Create with list of lines
    Organiser(List<String[]> fileLines){
        this.fileLines = fileLines;
    }

    /**
     * Takes each line of stored file and stores data in triangles
     */
    void sort(){
        for (String[] line : fileLines) {
            //check validity
            if (!checkValidity(line)) continue;

            //Split line
            String company = line[0];
            Integer originYear = Integer.parseInt(line[1]);
            Integer devYear = Integer.parseInt(line[2]);
            Double payment = Double.parseDouble(line[3]);
            t.track(originYear, devYear);

            //Pass into correct triangle
            if (checkTriangle(company)) {
                //Triangle already created
                //Check if origin year created
                if (triangles.get(company).containsKey(originYear)) {
                    triangles.get(company).get(originYear).addDevYear(devYear, payment);
                    //This is now adding a new payment to the Origin object here
                } else {
                    //Create new origin year
                    triangles.get(company).put(originYear, new Origin(originYear, devYear, payment));
                }
            }
            else {
                //Create new HashMap to add to triangles
                HashMap<Integer, Origin> newMap = new HashMap<>();
                newMap.put(originYear, new Origin(originYear, devYear, payment));
                triangles.put(company, newMap);
            }
            //Increment counter
            System.out.println("Counter: " + counter);
            counter++;
        }
        System.out.println("Lines sorted");
    }


    void output(){
        for (String s : triangles.keySet()) {
            System.out.println("HERE");

            List<String> compList = new ArrayList<>();
            compList.add(s);

            HashMap<Integer, Origin> innerMap = triangles.get(s);
            Set<Integer> keyOri = innerMap.keySet();

            for (Integer k : keyOri) {
                List<String> sum = innerMap.get(k).sumUp();
                compList.addAll(sum);
            }

            System.out.println(compList);
        }
    }

    /**
     * Checking that the line is of correct format
     * @param line being read
     * @return True iff line is correct format
     */
    private boolean checkValidity(String[] line) {
        //check line only contains four elements
        if(line.length != 4) {
            System.out.println("Line " + counter + " invalid");
            return false;
        }
        //check that the second, third and fourth elements of line are numbers
        try{
            Integer one = Integer.parseInt(line[1]);
            Integer on  = Integer.parseInt(line[2]);
            Double o   = Double.parseDouble(line[3]);
        }
        catch (NumberFormatException e){
            System.out.println("Line " + counter + " invalid");
            System.out.println("NumberFormatException " + e.getMessage());
            return false;
        }
        catch (Exception r){
            r.printStackTrace();
        }
        return true;
    }

    //Check if triangle already created
    private boolean checkTriangle(String line){
        return triangles.containsKey(line);
    }
}
